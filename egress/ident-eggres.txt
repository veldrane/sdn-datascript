### Namespace with certain id

[root@dl380 current]# oc get netnamespace | grep webapp
webapp                              544457     [10.16.1.225]
                                    ------
                                      |
                                      -> 544457 DEC = 0x084ec8 HEX !


### hostsubnet definition

[root@dl380 current]# oc get hostsubnet
NAME               HOST               HOST IP      SUBNET          EGRESS CIDRS       EGRESS IPS
node11.lab.local   node11.lab.local   10.16.1.11   10.48.11.0/24   []                 []
node17.lab.local   node17.lab.local   10.16.1.17   10.48.17.0/24   [10.16.1.224/27]   [10.16.1.225]
node18.lab.local   node18.lab.local   10.16.1.18   10.48.18.0/24   [10.16.1.224/27]   []
node21.lab.local   node21.lab.local   10.16.1.21   10.48.21.0/24   []                 []
node22.lab.local   node22.lab.local   10.16.1.22   10.48.22.0/24   []                 []
node23.lab.local   node23.lab.local   10.16.1.23   10.48.23.0/24   []                 []
node24.lab.local   node24.lab.local   10.16.1.24   10.48.24.0/24   []                 []




### Ovs dumpflows

on node17.lab.local

cookie=0x0, table=100, priority=100,ip,reg0=0x84ec9 actions=set_field:d2:a6:db:a6:a4:d1->eth_dst,set_field:0x1084ec8->pkt_mark,goto_table:101
					    -------                   -----------------                    ---------
					      |                               |                                |
                                              -> take packet with this        -> set dest mac to tun0          -> add 1 preffix and set the packet mark
					         vnid				 on node where ip 
                                                                                 of the egress is 
                                                                                 placd - same node


### Iptables Flow

-A OPENSHIFT-MASQUERADE -s 10.48.0.0/16 -m mark --mark 0x1084ec8 -j SNAT --to-source 10.16.1.225
						       ---------
                                                           |
                                                           -> snat to source 10.16.1.225 with all
							      packets with this mark

